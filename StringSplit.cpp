std::vector<std::string> split(std::string s, std::string& delimeter) {
	std::vector<std::string> ret;

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimeter)) != std::string::npos) {
		token = s.substr(0, pos);
		ret.emplace_back(token);
		s.erase(0, pos + delimeter.length());
	}
	ret.emplace_back(s);
	return ret;
}

/* input is not modified */
std::vector<std::string> splitu(const std::string& s, std::string& delimeter) {
	size_t last = 0;
	size_t next = 0;
	std::vector<std::string> ret;
	while ((next = s.find(delimeter, last)) != std::string::npos) {
		ret.emplace_back(s.substr(last, next - last));
		last = next + delimeter.size();
	}
	ret.emplace_back(s.substr(last));
	return ret;
}

// from hackerrank
vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
int main()
{
	std::string s = "fafdas//wefwef//dfsfd";
	std::string delimeter = "//";
	std::vector<std::string> ret = split(s, delimeter);
	for (size_t i = 0; i < ret.size(); ++i){
		std::cout << ret[i] << std::endl;
    }

	s = "fafdas//wefwef//dfsfd";
	delimeter = "//";
	ret = splitu(s, delimeter);
	for (size_t i = 0; i < ret.size(); ++i) {
		std::cout << ret[i] << std::endl;
	}
	// getchar();
}