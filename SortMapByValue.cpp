#include "stdafx.h"

#include <vector>
#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <string>
#include <vector>
#include<cstdlib>
#include <assert.h>
#include <locale>
#include <map>
#include <algorithm>
#include <set>
#include <iostream>
#include <string>
#include <stdexcept>
#include <list>
#include <vector>
#include <iterator>
#include <functional> 
#include <ctype.h>

using namespace std;

template<typename A, typename B>
std::pair<B, A> flip_pair(const std::pair<A, B> &p)
{
	return std::pair<B, A>(p.second, p.first);
}

template<typename A, typename B>
std::multimap<B, A, std::greater<B>> flip_map(const std::map<A, B> &src)
{
	std::multimap<B, A, std::greater<B>> dst;
	std::transform(src.begin(), src.end(), std::inserter(dst, dst.begin()),
		flip_pair<A, B>);
	return dst;
}


std::vector<std::string> split(std::string s, std::string& delimeter) {
	std::vector<std::string> ret;

	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimeter)) != std::string::npos) {
		token = s.substr(0, pos);
		ret.emplace_back(token);
		s.erase(0, pos + delimeter.length());
	}
	ret.emplace_back(s);
	return ret;
}

/* input is not modified */
std::vector<std::string> splitu(const std::string& s, std::string& delimeter) {
	size_t last = 0;
	size_t next = 0;
	std::vector<std::string> ret;
	while ((next = s.find(delimeter, last)) != std::string::npos) {
		ret.emplace_back(s.substr(last, next - last));
		last = next + delimeter.size();
	}
	ret.emplace_back(s.substr(last));
	return ret;
}

vector<string> retrieveMostFreqWords(string s, vector<string> ex) {
	vector<string> ret;
	int N = s.size();
	for (int i = 0; i < N; ++i) {
		if (s[i] >= 'A' && s[i] <= 'Z') {
			s[i] = tolower(s[i]);
		}
		else if (s[i] >= 'a' && s[i] <= 'z') {
			continue;
		}
		else {
			s[i] = ' ';
		}
	}
	string delimeter = " ";
	vector<string> tmp = split(s, delimeter);
	map<string, int> m;
	set<string> ss;
	for (int i = 0; i < tmp.size(); ++i) {
		m[tmp[i]]++;
		cout << tmp[i] << endl;
	}

	for (auto iter = m.begin(); iter != m.end(); ++iter) {
		cout << iter->first << ' ' << iter->second << endl;

	}
	for (int i = 0; i < ex.size(); ++i) {
		for (int j = 0; j < ex[i].size(); ++j){
			ex[i][j] = tolower(ex[i][j]);
		}
		ss.insert(ex[i]);
	}
	for (auto iter = ss.begin(); iter != ss.end(); ++iter) {
		cout << *iter << endl;

	}
	for (auto iter = m.begin(); iter != m.end(); ++iter) {
		if (ss.find(iter->first) != ss.end() || iter->first == " ") {
			m[iter->first] = -1;
		}
	}
	multimap<int, string, greater<int>> flip = flip_map(m);
	int mostfreq = flip.cbegin()->first;
	cout << "sfsfds" << mostfreq << endl;
	for (auto iter = flip.cbegin(); iter != flip.cend(); ++iter){
		if (mostfreq > iter->first) {
			break;
		}
		ret.push_back(iter->second);
	}

	return ret;
}
int main()
{
    /*the most frequently used words in the text.
      words that have a different case are counted as the same word.
      the order of the words doesn't matter in the output list.
      all words in the ex list are unique.
      any character other then leeters from the english alphabet should be treasted as white space*/
	std::string s = "Jack and Jill went to the market to buy bread and cheese. Cheese is Jack's and Jill's favorate food.";
	std::vector<std::string> ex{ "and", "he", "the", "to", "is", "Jack", "Jill" };
	std::vector<string> expected{ "cheese", "s" };
	vector<string> ret = retrieveMostFreqWords(s, ex);
	cout << "answer:" << endl;
	for (int i = 0; i < ret.size(); ++i){
		
		cout << ret[i] << endl;
}
	getchar();
}