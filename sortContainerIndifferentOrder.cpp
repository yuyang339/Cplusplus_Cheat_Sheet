sort(numbers.begin(), numbers.end(), [](const int a, const int b) {return a > b; });


struct greater
{
    template<class T>
    bool operator()(T const &a, T const &b) const { return a > b; }
};

std::sort(numbers.begin(), numbers.end(), greater());